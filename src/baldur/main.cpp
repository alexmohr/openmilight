#include <cstring>
#include <fstream>
#include <iomanip>
#include <iomanip>
#include <iostream>
#include <iostream>
#include <streambuf>
#include <string>
#include <thread>

// defines the GPIO pins
#include <RF24/utility/RPi/bcm2835.h>

#include "jsonServer.h"
#include "main.h"
#include "openmili.h"
#include "udp_milight.h"

using namespace std;

// defines the parameters of the configuration file.
struct conf_args {
    static constexpr const char* tcp_port = "tcp_port";
    static constexpr const char* pin_select = "pin_select";
    static constexpr const char* pin_enable = "pin_enable";

    static constexpr const char* udp_enabled = "udp_enabled";
    static constexpr const char* udp_remote = "udp_remote";
};

struct _exitCodes {
    static const uint32_t configError = 0xcf9;
    static const uint32_t networkError = 0xf411;
    static const uint32_t radioError = 0x01;
};

bool fileExists(const string& name)
{
    ifstream f(name.c_str());
    return f.good();
}

int main(int argc, char* argv[])
{
    const string cfgName = "baldur.conf";
    string configFile = cfgName;
    if (!fileExists(configFile)) {
        configFile = "../cfg/" + cfgName;
        if (!fileExists(configFile)) {
            configFile = "/etc/baldur/" + cfgName;
            if (!fileExists(configFile)) {
                cout << "Could not find config, exiting." << endl;
                exit(_exitCodes::configError);
            }
        }
    }

    // get content of configuration file.
    ifstream fileStream(configFile);
    string configContent((istreambuf_iterator<char>(fileStream)),
        istreambuf_iterator<char>());

    // parse it to json.
    // but watch out for errors.
    json cfg;
    try {
        cfg = json::parse(configContent);
    } catch (invalid_argument ex) {
        cout << "Configuration " << configFile << " contains errors. exiting"
             << endl;
        exit(_exitCodes::configError);
    }

    // get the settings
    uint16_t port = 2244;
    uint8_t pinEnable = RPI_V2_GPIO_P1_22;
    uint8_t pinSelect = RPI_V2_GPIO_P1_24;
    bool open = true;
    bool debug = false;

    if (cfg.count(conf_args::tcp_port) != 1)
        cout << "port not configured. Using fallback: " << to_string(port) << endl;
    else
        port = cfg[conf_args::tcp_port];

    if (cfg.count(conf_args::pin_select) != 1)
        cout << "select pin not configured. using fallback.: "
             << to_string(pinSelect) << endl;
    else
        pinSelect = cfg[conf_args::pin_select];

    if (cfg.count(conf_args::pin_enable) != 1)
        cout << "enable pin not configured. using fallback.: "
             << to_string(pinEnable) << endl;
    else
        pinEnable = cfg[conf_args::pin_enable];

    // parse command line arguments; they may overwrite the configuration.
    for (int i = 0; i < argc; i++) {
        if (string(commandlineArgs::nopen) == string(argv[i]))
            open = false;
        else if (string(commandlineArgs::help) == string(argv[i])) {
            cout << commandlineArgs::help_text << endl;
            exit(_exitCodes::configError);
        } else if (string(commandlineArgs::debug) == string(argv[i])) {
            debug = true;
        } else if (string(commandlineArgs::port) == string(argv[i])) {
            if (++i >= argc) {
                cout << "Port arguments needs data" << endl;
                exit(_exitCodes::configError);
            }

            bool parsed = JsonServer::tryParseInt(argv[i], port);
            if (!parsed) {
                cout << "the given data of " << argv[i] << "is not a valid int" << endl;
            }
        }
    }

    // set chip enable (CE) and chip select not (CSN)
    //  GPIO pins on pi in terms of the underlying BCM GPIO pin numbers
    RF24 radio(
        pinEnable, pinSelect,
        BCM2835_SPI_SPEED_8MHZ); // clock speed will be changed by milight impl

    OpenMilight* milight = new OpenMilight(radio, debug);
    if (open) {
        int res = milight->Open();
        if (-1 == res) {
            cout << "Failed to open radio" << endl;
            exit(_exitCodes::configError);
        }
    }

    cout << "Starting server on port " << port << endl;
    JsonServer* srv = new JsonServer(milight, debug);

    if (srv->Open(port) != 0) {
        cout << "failed to open." << endl;
        exit(_exitCodes::networkError);
    } /**/
    if (cfg.count(conf_args::udp_enabled) == 1) {
        if (cfg[conf_args::udp_enabled] == true)
            if (cfg.count(conf_args::udp_remote) == 1) {
                uint16_t remoteid = 0;
                JsonServer::jsonArgToInt(cfg[conf_args::udp_remote], remoteid);
                udp_milight* udp = new udp_milight(remoteid, milight, debug);

                bool live = true;
                udp->serve(live);
            }
    }

    while (true)
        sleep(10 * 1000);
}
