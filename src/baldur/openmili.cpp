/*
 * Author: Alexander Mohr
 * Copyright: GPL V3
 *
 * Based on the work of Torsten Traenker
 *
 *
 */

#include "openmili.h"

OpenMilight::OpenMilight(bool debug)
{
    _debug = debug;
    _isOpen = false;
    _lastBrightness = 0;
    _lastColor = 0;
}

///
/// \brief OpenMilight::OpenMilight creates a new instance of the milight
/// handler
/// \param pinEnable Address of the GPIO pin where the enabled pin is connected
/// \param pinSelect Address of the GPIO pin where the select pin is connected
///
OpenMilight::OpenMilight(RF24& radio, bool debug)
    : OpenMilight(debug)
{
    //! \todo make this handler configurable
    _radioHandler = new PL1167_nRF24(radio);
    _miLightRadio = new MiLightRadio(*_radioHandler);
    _sequence = 1;
}

OpenMilight::~OpenMilight()
{
    delete (_radioHandler);
    delete (_miLightRadio);
}

int OpenMilight::Open()
{
    int radioResult = _miLightRadio->begin();
    if (0 > radioResult) {
        return radioResult;
    }

    _isOpen = true;
    return 0;
}

void OpenMilight::Close()
{
    _isOpen = false;
}

bool OpenMilight::send(uint8_t data[8], uint16_t resends)
{
    if (_debug) {
        printf("2.4GHz --> Sending: ");
        for (int i = 0; i < 7; i++) {
            printf("%02X ", data[i]);
        }
        printf(" [x%d]\n", resends);
    }

    _lastBrightness = data[4];
    _lastColor = data[3];

    if (!_isOpen)
        return false;

    if (data[6] == 0x00) {
        data[6] = ++_sequence;
    }

    _miLightRadio->write(data, _packetLength);

    for (int i = 0; i < resends; i++) {
        _miLightRadio->resend();
    }
    return true;
}

bool OpenMilight::send(uint64_t v)
{
    uint8_t data[8];
    data[7] = (v >> (7 * 8)) & 0xFF; // resends
    data[0] = (v >> (6 * 8)) & 0xFF; // remote 0
    data[1] = (v >> (5 * 8)) & 0xFF; // remote 1
    data[2] = (v >> (4 * 8)) & 0xFF; // remote 2
    data[3] = (v >> (3 * 8)) & 0xFF; // color
    data[4] = (v >> (2 * 8)) & 0xFF; // brightness
    data[5] = (v >> (1 * 8)) & 0xFF; // key
    data[6] = (v >> (0 * 8)) & 0xFF; // sequence

    return send(data, data[7]);
}

bool OpenMilight::send(uint8_t color, uint8_t bright, uint8_t key, uint8_t remote,
    uint8_t remote_prefix, uint8_t prefix, uint8_t seq,
    uint16_t resends)
{
    uint8_t data[8];
    data[0] = prefix;
    data[1] = remote_prefix;
    data[2] = remote;
    data[3] = color;
    data[4] = bright;
    data[5] = key;
    data[6] = seq;
    data[7] = resends;

    return send(data, resends);
}

uint8_t OpenMilight::getBrightness()
{
    return _lastBrightness;
}

uint8_t OpenMilight::getColor()
{
    return _lastColor;
}

///
/// \brief ReceivePackage receive a package from the internal buffer.
/// \return an item from the recv buffer.
///
void OpenMilight::ReceivePackages(unsigned long& packageSize, int msRecvtime, vector<uint8_t*>* outBuff)
{
    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);
    clock_gettime(CLOCK_MONOTONIC, &end);
    end.tv_nsec += msRecvtime * 1E6;
    end.tv_sec += msRecvtime;

    while (start.tv_sec < end.tv_sec) {
        if (_miLightRadio->available()) {
            uint8_t packet[7];
            size_t packet_length = packageSize;
            _miLightRadio->read(packet, packet_length);

            outBuff->push_back(packet);
        }
    }
}
