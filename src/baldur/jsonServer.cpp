#include "jsonServer.h"
#include <arpa/inet.h>
#include <exception>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

JsonServer::JsonServer(OpenMilight* milight, bool debug)
{
    _debug = debug;
    _stopListening = false;
    _milight = milight;

    _runningClientHandler = new vector<thread*>();

    // mininum brightess from api
    _lastBrightness = 0;
}

JsonServer::~JsonServer()
{
    delete (_runningClientHandler);
}

// stupid helper function
// todo maybe move that to the class definitio.
void* get_in_addr(struct sockaddr* sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int JsonServer::Open(uint tcpPort)
{
    _stopListening = false;
    // Variables for writing a server.
    /*
    1. Getting the address data structure.
    2. Openning a new socket.
    3. Bind to the socket.
    4. Listen to the socket.
    5. Accept Connection.
    6. Receive Data.
    7. Close Connection.
    */
    int status;
    struct addrinfo hints, *res;
    //int _listner;

    // Before using hint you have to make sure that the data structure is empty
    memset(&hints, 0, sizeof hints);
    // Set the attribute for hint
    hints.ai_family = AF_UNSPEC; // We don't care V4 AF_INET or 6 AF_INET6
    hints.ai_socktype = SOCK_STREAM; // TCP Socket SOCK_DGRAM
    hints.ai_flags = AI_PASSIVE;

    string port = std::to_string(tcpPort);

    // Fill the res data structure and make sure that the results make sense.
    status = getaddrinfo(NULL, port.c_str(), &hints, &res);
    if (status != 0) {
        cerr << "getaddrinfo error: " << gai_strerror(status) << endl;
        ;
        return status;
    }

    // Create Socket and check if error occured afterwards
    _listner = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (_listner < 0) {
        cerr << "socket error " << gai_strerror(status) << endl;
        return status;
    }
    // Bind the socket to the address of my local machine and port number
    status = bind(_listner, res->ai_addr, res->ai_addrlen);
    if (status < 0) {
        cerr << "bind error " << gai_strerror(status) << endl;
        close(_listner);
        return status;
    }

    status = listen(_listner, 10);
    if (status < 0) {
        cerr << "listen error " << gai_strerror(status);

        close(_listner);
        return status;
    }

    // Free the res linked list after we are done with it
    freeaddrinfo(res);

    struct timeval timeout;
    timeout.tv_usec = 0;
    timeout.tv_sec = 30;

    if (setsockopt(_listner, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) < 0) {
        cout << "set timeout failed" << endl;
        return false;
    }

    if (setsockopt(_listner, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) < 0) {
        cout << "set timeout failed" << endl;
        return status;
    }

    // reuse allows us to do open and close without the os preventing this.
    int enable = 1;
    if (setsockopt(_listner, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0 || setsockopt(_listner, SOL_SOCKET, SO_REUSEPORT, &enable, sizeof(int)) < 0) {
        cout << "set reuse option failed" << endl;
        return status;
    }

    _clientListener = thread(&JsonServer::ListenerLoop, this);

    return 0;
}

void JsonServer::Close()
{
    _stopListening = true;

#if false
      _clientHandlerLock.lock();

      for (int i = 0; i < _runningClientHandler->size(); i++){
          thread *client = _runningClientHandler->at(i);
          client->join();
      }

      _clientHandlerLock.unlock();
#endif
    close(_listner);
    _clientListener.join();
}

void JsonServer::ListenerLoop()
{
    // We should wait now for a connection to accept
    int new_conn_fd;
    struct sockaddr_storage client_addr;
    socklen_t addr_size;
    char address[INET6_ADDRSTRLEN]; // an empty string

    // Calculate the size of the data structure
    addr_size = sizeof client_addr;

    while (!_stopListening) {

        // check for dead threads.
        //_clientHandlerLock.lock();

        for (uint32_t i = 0; i < _runningClientHandler->size(); i++) {
            thread* client = _runningClientHandler->at(i);
            if (!client->joinable())
                continue;

            if (_debug)
                cout << "-- Ending a client thread... " << endl;
            client->join();
            delete (client);
            _runningClientHandler->erase(_runningClientHandler->begin() + i);
        }
        // go go gadget client handler.

        //_clientHandlerLock.unlock();

        // Accept a new connection and return back the socket desciptor
        // wait for a new client to connect
        new_conn_fd = accept(_listner, (struct sockaddr*)&client_addr, &addr_size);
        if (new_conn_fd < 0) {
            continue;
        }
        if (_debug)
            cout << "New client connected." << endl;
        inet_ntop(client_addr.ss_family, get_in_addr(
                                             (struct sockaddr*)&client_addr),
            address,
            sizeof address);

        _clientHandlerLock.lock();

        // go go gadget client handler.
        thread* clientThread = new thread(&JsonServer::HandleClient, this, new_conn_fd);
        _runningClientHandler->push_back(clientThread);

        _clientHandlerLock.unlock();

        // get the data from the client.
        //HandleClient(new_conn_fd);
    }
}

void JsonServer::HandleClient(int new_conn_fd)
{
    // handle the client.
    while (!_stopListening) {
        string data;
        if (_debug)
            cout << "Waiting for data..." << endl;

        int status = GetLineFromBuffer(new_conn_fd, data);
        if (status != 0 || data.size() < 1) {
            // close the connection and handle the next client.
            if (_debug)
                cout << "Client disconnected forecefully, no data or timeout reached." << endl;
            close(new_conn_fd);
            break;
        }

        // init response with an empty string
        string response = "";

        // parse the request.
        if (_debug)
            cout << "Parsing input: " << data << endl;
        response = ParseData(data);
        if (response.empty()) {
            response = json_resp::internal_error;
        }

        char lastChar = response.back();

        // append termination char.
        if ('\n' != lastChar)
            response += "\n";

        // answer the client
        // ssize_t send(int sockfd, const void *buf, size_t len, int flags);
        if (_debug)
            cout << "Sending data to client: " << response << endl;

        // todo handle result
        send(new_conn_fd, response.c_str(), response.size(), 0);
    }
}

int JsonServer::GetLineFromBuffer(int sockfd, string& data)
{
    // define an input buffer.
    std::vector<char>* input_buf = new std::vector<char>();

    char input;
    do {
        // read a single byte
        int ret = read(sockfd, &input, 1);
        if (ret < 1) {
            return ret;
        }

        input_buf->push_back(input);
    } while (input != '\n');

    // create an std string from the input buffer.
    data = string(input_buf->begin(), input_buf->end());

    // because we do not need the original buffer anymore; delete it.
    delete (input_buf);

    return 0;
}
string JsonServer::ParseData(string input)
{

    // define a string we'll send back to the client
    json response;
    json jay;
    // turn the string into a json structure.
    // see https://github.com/nlohmann/json
    // thanks! this thing is awesome
    try {
        jay = json::parse(input);
    } catch (invalid_argument ex) {
        response[json_args::response] = json_resp::malformed;
        return response.dump();
    }

    // make sure we have gotten a remote id.
    if (jay.count(json_args::remote) != 1 || jay[json_args::remote] == nullptr) {

        response[json_args::response] = json_resp::missing_remote;
        return response.dump();
    }

    // check if we have a command.
    if (jay.count(json_args::command) != 1 || jay[json_args::command] == nullptr) {

        response[json_args::response] = json_resp::missing_cmd;
        return response.dump();
    }

    // defines the command.
    command cmd;
    cmd.remoteId = 0;
    cmd.commandId = 0;
    cmd.color = 0;
    _lastSequenceId += 5;
    cmd.sequenceId = _lastSequenceId;

    // make sure we can handle int and strings
    json remote = jay[json_args::remote];
    json command = jay[json_args::command];

    if (!jsonArgToInt(remote, cmd.remoteId)) {
        response[json_args::response] = json_resp::malformed_remote;
        return response.dump();
    }

    // command parsing here is kind of dull ...
    // handle the commana as string or int
    if (command.type() == json::value_t::string) {
        if (_commands.count(command)) {
            cmd.commandId = _commands.at(command);
        } else {
            response[json_args::response] = json_resp::malformed_cmd;
        }
    } else if (!jsonArgToInt(command, cmd.commandId)) {
        response[json_args::response] = json_resp::malformed_cmd;
        return response.dump();
    }

    if (0 == cmd.commandId) {
        response[json_args::response] = json_resp::malformed_cmd;
        return response.dump();
    }

    // check if we have a color.
    if (jay.count(json_args::color) > 0 && jay[json_args::color] != nullptr) {

        if (!jsonArgToInt(jay[json_args::color], cmd.color)) {
            response[json_args::response] = json_resp::malformed_color;
            return response.dump();
        }
    }

    // check if we have a brightness.
    if (jay.count(json_args::brightness) > 0 && jay[json_args::brightness] != nullptr) {

        if (!jsonArgToInt(jay[json_args::brightness], cmd.brightness)) {
            response[json_args::response] = json_resp::malformed_brightness;
            return response.dump();
        }

        // _lastBrightness = cmd.brightness;
    }

    // check if we have a send count.
    if (jay.count(json_args::resend) > 0 && jay[json_args::resend] != nullptr) {

        if (!jsonArgToInt(jay[json_args::resend], cmd.resend)) {
            response[json_args::response] = json_resp::malformed_cmd;
            return response.dump();
        }
    }

    // handle protocoll extensions
    // check if we have a send count.
    bool send = true;
    uint8_t val = 0;
    switch (cmd.commandId) {
    case brighter:
        if (_lastBrightness < _maxBrightness)
            _lastBrightness += _brightnessIncrease;
        cmd.commandId = brightness;
        break;
    case darker:
        if (_lastBrightness > _minBrightness)
            _lastBrightness -= _brightnessIncrease;
        cmd.commandId = brightness;
        break;
    case brightness:
        _lastBrightness = cmd.brightness;
        break;
    case get_brightness:
        val = _lastBrightness;
        // todo use this:
        // val = _milight->getBrightness();
        send = false;
        break;
    case get_color:
        val = _milight->getColor();
        send = false;
    }

    if (send) {
        if (_lastBrightness > _maxBrightness)
            _lastBrightness = _maxBrightness;
        else if (_lastBrightness < _minBrightness)
            _lastBrightness = _minBrightness;

        uint8_t myval = ((0x90 - ((_lastBrightness + 2) * 8) + 0x100) & 0xFF) | (0 & 0x07);
        uint8_t oval = ((0x90 - ((_lastBrightness + 2) * 8) + 0x100) & 0xFF);

        std::cout << myval;
        std::cout << oval;

        cmd.brightness = ((0x90 - ((_lastBrightness + 2) * 8) + 0x100) & 0xFF) | (0 & 0x07);

        bool success = sendCommand(cmd);
        if (!success)
            response[json_args::response] = json_resp::send_failed;
        else
            response[json_args::response] = json_resp::sucess;
    } else {
        response[json_args::response] = val;
    }

    return response.dump();
}

bool JsonServer::jsonArgToInt(json jay, uint8_t& value)
{
    bool retVal = false;
    if (jay.type() == json::value_t::number_integer) {
        value = jay;
        retVal = true;
    } else if (jay.type() == json::value_t::number_float || jay.type() == json::value_t::number_unsigned) {
        value = (int)jay;
        retVal = true;
    } else {
        retVal = tryParseInt(jay, value);
    }

    return retVal;
}

bool JsonServer::jsonArgToInt(json jay, uint16_t& value)
{
    bool retVal = false;
    if (jay.type() == json::value_t::number_integer) {
        value = jay;
        retVal = true;
    } else if (jay.type() == json::value_t::number_float || jay.type() == json::value_t::number_unsigned) {
        value = (int)jay;
        retVal = true;
    } else {
        retVal = tryParseInt(jay, value);
    }

    return retVal;
}

bool JsonServer::sendCommand(JsonServer::command cmd)
{
    static uint8_t outgoingPacket[7];
    memset(outgoingPacket, 0, 7);

    /*
     * uint8_t color,
     * uint8_t bright,
     * uint8_t key,
     * uint8_t remote,
     * uint8_t remote_prefix,
     * uint8_t prefix,
     * uint8_t seq,
     * uint16_t resends)
     * */

    return _milight->send(
        cmd.color,
        cmd.brightness,
        cmd.commandId,
        cmd.remoteId & 0xff,
        (cmd.remoteId >> 8),
        cmd.remotePrefix,
        cmd.sequenceId,
        cmd.resend);
}

uint8_t* JsonServer::toUint8Array(int input)
{
    uint8_t* bytes = new uint8_t[sizeof input];
    std::copy(static_cast<const char*>(static_cast<const void*>(&input)),
        static_cast<const char*>(static_cast<const void*>(&input)) + sizeof input,
        bytes);

    return bytes;
}

bool JsonServer::tryParseInt(const string input, uint8_t& output)
{
    uint16_t tmp = (uint16_t)output;
    bool retVal = tryParseInt(input, tmp);
    output = (uint8_t)tmp;

    return retVal;
}

bool JsonServer::tryParseInt(const string input, uint16_t& output)
{
    if (input.empty())
        return false;

    int input_size = input.size();

    // define iterator index.
    int startIndex = 0;
    int base = 10;

    if (input_size > 2) {
        if (input.at(0) == '0' && (input.at(1) == 'x' || input.at(1) == 'X')) {
            startIndex = 2;
            base = 16;
        }
    }

    // itterate over the string to validate each
    // char if it is a valid number.
    if (base == 16)
        for (uint32_t i = startIndex; i < input.size(); i++)
            if (!isdigit(input[i]))
                if (!isxdigit(input[i]))
                    return false;

    try {
        output = std::stoul(input, nullptr, base);
        return true;
    } catch (invalid_argument) {
        return false;
    }
}
