
#ifndef OpenMilight_H_
#define OpenMilight_H_

#include <cstdlib>
#include <iostream>
#include <map>
#include <mutex>
#include <string.h>
#include <thread>
#include <unistd.h>
#include <vector>

using namespace std;
#include <RF24/RF24.h>
#include <RF24/utility/RPi/bcm2835.h>

#include "MiLightRadio.h"
#include "PL1167_nRF24.h"

const uint8_t _packetLength = 7;
///
/// \brief OpenMilight handler. Lowest handling class before device drivers.
///
class OpenMilight {

private:
    uint8_t _sequence;

    bool _debug;

    ///
    /// \brief The actual radio driver which does the SPI communication
    ///
    //RF24* _radio;

    ///
    /// \brief Generic device handler and middleware between driver and milight
    /// interface
    ///
    IRadio* _radioHandler;

    ///
    /// \brief _miLightRadio Milight protocoll implementation.
    ///
    MiLightRadio* _miLightRadio;

    ///
    /// \brief Flag that tells this has been openend and we are listening
    /// to the milight radio.
    ///
    bool _isOpen;

    /// \brief _radioMutex used to access the radio instances thread safe
    ///
    mutex _radioMutex;

    ///
    /// \brief _maxBufferSize Defines the maximum number of messages in the recv
    /// buffer.
    ///
    const uint16_t _maxBufferSize = 10 * 1000;

    uint8_t _lastBrightness = 0;
    uint8_t _lastColor = 0;

public:
    /**
   * @brief This will create a milight object without radio
   */
    OpenMilight(bool debug = false);

    ///
    /// \brief OpenMilight::OpenMilight creates a new instance of the milight
    /// handler
    /// \param pinEnable Address of the GPIO pin where the enabled pin is
    /// connected
    /// \param pinSelect Address of the GPIO pin where the select pin is connected
    ///
    OpenMilight(RF24& radio, bool debug = false);

    ///
    /// \brief Destructs this object
    ///
    ~OpenMilight();

    ///
    /// \brief Starts the milight communication
    ///
    int Open();

    ///
    /// \brief Stops the milight communication
    ///
    void Close();

    bool send(uint8_t data[8], uint16_t resend);

    bool send(uint64_t v);

    bool send(uint8_t color, uint8_t bright, uint8_t key, uint8_t remote = 0x01,
        uint8_t remote_prefix = 0x00, uint8_t prefix = 0xB0,
        uint8_t seq = 0x00, uint16_t resends = 10);

    uint8_t getBrightness();

    uint8_t getColor();

    ///
    /// \brief ReceivePackage receive a package from the internal buffer.
    /// \return true if the package has been filled, otherwise false
    ///
    void ReceivePackages(unsigned long& packageSize, int msRecvtime, vector<uint8_t*>* outBuff);
};
#endif
