struct commandlineArgs {

    ///
    /// \brief Give this argument to prevent opening of device.
    ///
    static constexpr const char* nopen = "--nopen";

    ///
    /// \brief Give this argument to set a port
    ///
    static constexpr const char* port = "--port";

    ///
    /// \brief Give this argument to print help
    ///
    static constexpr const char* help = "--help";

    ///
    /// \brief enables debug prints
    ///
    static constexpr const char* debug = "--debug";

    static constexpr const char* help_text = "baldur openmilight remote service (c) 2016 A. Mohr GPLV3\n\n"
                                             "--port\t Defines the json server listening port; overwriting the configuration file\n"
                                             "--nopen\t Not Open. Prevents the startup of the radio module.\n"
                                             "--help\t Prints this help\n";
};
