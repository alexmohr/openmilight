#include <algorithm>
#include <thread>

#include "json.hpp"
#include "openmili.h"

// for convenience
using json = nlohmann::json;

using namespace std;

const char _brightnessIncrease = 1;
const uint32_t _minBrightness = 0;
const uint32_t _maxBrightness = 25;

///
/// \brief Enumeration which defines the different hex values for the command
/// which are understood by milight.
///
enum command_values {
    all_off = 0x02,
    all_on = 0x01,
    z1_on = 0x03,
    z1_off = 0x04,
    z2_on = 0x05,
    z2_off = 0x06,
    z3_on = 0x07,
    z3_off = 0x08,
    z4_on = 0x09,
    z4_off = 0x0A,
    disco_faster = 0x0B,
    disco_slower = 0x0C,
    disco_change = 0x0D,
    brightness = 0x0E,
    color = 0x0F,
    all_white = 0x11,
    z1_white = 0x13,
    z2_white = 0x15,
    z3_white = 0x17,
    z4_white = 0x19,
    all_night = 0x12,
    z1_night = 0x14,
    z2_night = 0x16,
    z3_night = 0x18,
    z4_night = 0x1A,
    none = 0x00,

    // extension to original protocol.
    brighter = 0xff,
    darker = 0xfe,
    get_brightness = 0xfd,
    get_color = 0xfc
};

///
/// \brief Maps the command text to the command value.
///
const map<string, command_values> _commands = {
    { "all_off", all_off },
    { "all_on", all_on },
    { "z1_on", z1_on },
    { "z1_off", z1_off },
    { "z2_on", z2_on },
    { "z2_off", z2_off },
    { "z3_on", z3_on },
    { "z3_off", z3_off },
    { "z4_on", z4_on },
    { "z4_off", z4_off },
    { "disco_faster", disco_faster },
    { "disco_slower", disco_slower },
    { "disco_change", disco_change },
    { "brightness", brightness },
    { "color", color },
    { "all_white", all_white },
    { "z1_white", z1_white },
    { "z2_white", z2_white },
    { "z3_white", z3_white },
    { "z4_white", z4_white },
    { "all_night", all_night },
    { "z1_night", z1_night },
    { "z2_night", z2_night },
    { "z3_night", z3_night },
    { "z4_night", z4_night },

    // protocol extension
    { "brighter", brighter },
    { "darker", darker },
    { "get_brightness", get_brightness },
    { "get_color", get_color },
};

///
/// \brief This structure defines constant string we find in the json.
///
struct json_args {
    static constexpr const char* remote = "remote";
    static constexpr const char* color = "color";
    static constexpr const char* brightness = "brightness";
    static constexpr const char* command = "command";
    static constexpr const char* sequence = "sequence";

    static constexpr const char* resend = "resend";
    static constexpr const char* response = "response";

    // aswell extensions to the protocoll.
    // allow to increase or decrease brightness without storing
    // any info in client.
    static constexpr const char* brighter = "brighter";
    static constexpr const char* darker = "darker";
};

///
/// \brief Contains data for responses
///
struct json_resp {
    ///
    /// \brief This defines a complete json, so we can just send it without further
    /// processing
    ///
    static constexpr const char* internal_error = "{\"response\":\"internal error\"}";

    static constexpr const char* missing_cmd = "CommandId missing";
    static constexpr const char* missing_remote = "RemoteId missing";
    static constexpr const char* missing_color = "Color missing";
    static constexpr const char* missing_brightness = "Brightness missing";

    static constexpr const char* sucess = "ok";
    static constexpr const char* send_failed = "Failed to send data to radio.";

    static constexpr const char* malformed = "Inparsable data";
    static constexpr const char* malformed_remote = "remote invalid";
    static constexpr const char* malformed_cmd = "cmd invalid";
    static constexpr const char* malformed_color = "color invalid";
    static constexpr const char* malformed_brightness = "brightness invalid";
};

class JsonServer {

    // Define constants
private:
    struct command {
        char remotePrefix = 0xB0;
        uint16_t remoteId = 0;
        uint8_t commandId = 0;
        uint8_t color = 0;
        uint8_t brightness = 0;
        uint8_t sequenceId = 0;
        uint16_t resend = 15;
    };

    // define the class fields.
private:
    ///
    /// \brief _milight holds the instance of the milight handler.
    ///
    OpenMilight* _milight;

    ///
    /// \brief Thread which waits for new clients to connect to the server
    /// will spawn a new thread foreach new client.
    ///
    thread _clientListener;

    ///
    /// \brief Helper field to tell us to abort the listening.
    ///
    bool _stopListening;

    int _listner;

    uint8_t _lastSequenceId = 0;

    uint8_t _lastBrightness = 0;

    bool _debug;

    mutex _clientHandlerLock;
    vector<thread*>* _runningClientHandler;

    const int _acceptTimeout = 5; // seconds

    // define private class methods.
private:
    string ParseData(string input);

    void ListenerLoop();

    bool sendCommand(JsonServer::command cmd);

    ///
    /// \brief Helper function for value conversion.
    ///
    uint8_t* toUint8Array(int input);

    ///
    /// \brief Handles the communication with a single tcp client.
    /// \param File descriptor of the client socket.
    ///
    void HandleClient(int new_conn_fd);

    // public methods, d'tor c'tor
public:
    ///
    /// \brief JsonServer handles commands request from the outside.2
    /// \param milight gives an instance to the milight handler
    /// \param tcpPort defines the tcp port on which the server will listen.
    ///
    JsonServer(OpenMilight* milight, bool debug = false);

    ~JsonServer();

    ///
    /// \brief Open will start the the tcp server and listens to connections.
    ///
    int Open(uint tcpPort);

    ///
    /// \brief Closes the servers and stops listening
    ///
    void Close();

    /**
    * @brief Gets a line from the tcp buffer.
    * @details Reads data from a file descriptor until a new line (\n) or a new line
    * with carriage return (\r\n) is received. Used to get json data.
    *
    * @param sockfd Defines the file descriptor the data is read from.
    *
    * @return The data we've read
    */
    int GetLineFromBuffer(int sockfd, string& data);

    ///
    /// \brief Tries to parse a string into an integer. Also works with hex numbers.
    /// \param The string to parse
    /// \param The output number.
    /// \return True if parsed; otherwise false
    ///
    static bool tryParseInt(const string input, uint16_t& output);

    static bool tryParseInt(const string input, uint8_t& output);

    ///
    /// \brief Converts a json part to integer, regargless of orginial type
    /// \param the json to parse
    /// \param the integer to fill
    /// \return True if successfull otherwise false
    ///
    static bool jsonArgToInt(json jay, uint8_t& value);
    static bool jsonArgToInt(json jay, uint16_t& value);
};
