#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/select.h>
#include <sys/time.h>

#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/socket.h>

// needed for build on raspbian
#ifndef _UNISTD_H
#include <unistd.h>
#endif
#ifndef _IOCTL_H_
#include <sys/ioctl.h>
#endif

using namespace std;

#include "openmili.h"
class udp_milight {
public:
    udp_milight(uint16_t remote, OpenMilight* light, bool debug = false);

    void open();
    void close();
    void serve(bool& keepserving);

private:
    OpenMilight* _light;
    bool _debug = false;
    uint16_t _remote;
};
