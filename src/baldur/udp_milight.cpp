#include "udp_milight.h"

udp_milight::udp_milight(uint16_t remote, OpenMilight* light, bool debug)
{
    _remote = remote;
    _light = light;
    _debug = debug;
}

void udp_milight::open()
{
    // make it threading?
}

void udp_milight::close()
{
}

void udp_milight::serve(bool& keepserving)
{
    fd_set socks;
    int discover_fd, data_fd;
    struct sockaddr_in discover_addr, data_addr, cliaddr;
    char mesg[42];

    // set default
    const uint8_t replySize = 30;
    const char hello[] = ",BABECAFEBABE";

    char reply[replySize];
    memset(reply, 0, replySize);

    int disco = -1;

    uint8_t data[8];
    data[0] = 0xB0; // seems like it does not matter?
    data[1] = (_remote >> 8) & 0xFF;
    data[2] = _remote & 0xFF;
    data[3] = 0x00;
    data[4] = 0x00;
    data[5] = 0x00;
    data[6] = 0x01;
    data[7] = 0x09;

    discover_fd = socket(AF_INET, SOCK_DGRAM, 0);
    bzero(&discover_addr, sizeof(discover_addr));
    discover_addr.sin_family = AF_INET;
    discover_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    discover_addr.sin_port = htons(48899);
    bind(discover_fd, (struct sockaddr*)&discover_addr, sizeof(discover_addr));

    data_fd = socket(AF_INET, SOCK_DGRAM, 0);
    bzero(&data_addr, sizeof(data_addr));
    data_addr.sin_family = AF_INET;
    data_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    data_addr.sin_port = htons(8899);
    bind(data_fd, (struct sockaddr*)&data_addr, sizeof(data_addr));

    long destination, gateway;
    char iface[IF_NAMESIZE];
    char buf[4096];

    FILE* file;

    memset(iface, 0, sizeof(iface));
    memset(buf, 0, sizeof(buf));

    file = fopen("/proc/net/route", "r");
    if (file) {
        while (fgets(buf, sizeof(buf), file)) {
            if (sscanf(buf, "%s %lx %lx", iface, &destination, &gateway) == 3) {
                if (destination == 0) { /* default */
                    int netfd;
                    struct ifreq ifr;

                    netfd = socket(AF_INET, SOCK_DGRAM, 0);

                    /* I want to get an IPv4 IP address */
                    ifr.ifr_addr.sa_family = AF_INET;

                    /* I want IP address attached to iface */
                    strncpy(ifr.ifr_name, iface, IFNAMSIZ - 1);

                    ioctl(netfd, SIOCGIFADDR, &ifr);
                    char* addr = inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr);
                    memcpy(&reply[0], addr, IF_NAMESIZE);

                    memcpy(&reply[strlen(reply)], hello, sizeof(hello));

                    fclose(file);
                    break;
                }
            }
        }
    }

    if (_debug) {
        printf("Reply String: %s\n", reply);
        fflush(stdout);
    }

    while (keepserving) {
        socklen_t len = sizeof(cliaddr);
        FD_ZERO(&socks);
        FD_SET(discover_fd, &socks);
        FD_SET(data_fd, &socks);

        if (select(FD_SETSIZE, &socks, NULL, NULL, NULL) >= 0) {

            if (FD_ISSET(discover_fd, &socks)) {
                int n = recvfrom(discover_fd, mesg, 41, 0, (struct sockaddr*)&cliaddr, &len);
                mesg[n] = '\0';

                if (_debug) {
                    char str[INET_ADDRSTRLEN];
                    long ip = cliaddr.sin_addr.s_addr;
                    inet_ntop(AF_INET, &ip, str, INET_ADDRSTRLEN);
                    printf("UDP --> Received discovery request (%s) from %s\n", mesg, str);
                }

                if (!strncmp(mesg, "Link_Wi-Fi", 41)) {
                    sendto(discover_fd, reply, replySize, 0, (struct sockaddr*)&cliaddr, len);
                }
            }

            if (FD_ISSET(data_fd, &socks)) {
                int n = recvfrom(data_fd, mesg, 41, 0, (struct sockaddr*)&cliaddr, &len);

                mesg[n] = '\0';

                if (n == 2 || n == 3) {
                    if (_debug) {
                        printf("UDP --> Received hex value (%02x, %02x, %02x)\n", mesg[0], mesg[1], mesg[2]);
                    }

                    switch ((uint8_t)mesg[0]) {
                    /* Color */
                    case 0x40:
                        disco = -1;
                        data[5] = 0x0F;
                        data[3] = (0xC8 - mesg[1] + 0x100) & 0xFF;
                        data[0] = 0xB0;
                        break;
                    /* All Off */
                    case 0x41:
                        data[5] = 0x02;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* All On */
                    case 0x42:
                        data[4] = (data[4] & 0xF8);
                        data[5] = 0x01;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Disco slower */
                    case 0x43:
                        data[5] = 0x0C;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Disco faster */
                    case 0x44:
                        data[5] = 0x0B;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Z1 On */
                    case 0x45:
                        data[4] = (data[4] & 0xF8) | 0x01;
                        data[5] = 0x03;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Z1 Off */
                    case 0x46:
                        data[5] = 0x04;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Z2 On */
                    case 0x47:
                        data[4] = (data[4] & 0xF8) | 0x02;
                        data[5] = 0x05;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Z2 Off */
                    case 0x48:
                        data[5] = 0x06;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Z3 On */
                    case 0x49:
                        data[4] = (data[4] & 0xF8) | 0x03;
                        data[5] = 0x07;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Z3 Off */
                    case 0x4A:
                        data[5] = 0x08;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Z4 On */
                    case 0x4B:
                        data[4] = (data[4] & 0xF8) | 0x04;
                        data[5] = 0x09;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Z4 Off */
                    case 0x4C:
                        data[5] = 0x0A;
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* Disco */
                    case 0x4D:
                        disco = (disco + 1) % 9;
                        data[0] = 0xB0 + disco;
                        data[5] = 0x0D;
                        break; /* Brightness */
                    case 0x4E:
                        data[5] = 0x0E;
                        data[4] = ((0x90 - (mesg[1] * 8) + 0x100) & 0xFF) | (data[4] & 0x07);
                        if (disco > 0) {
                            data[0] = 0xB0 + disco;
                        }
                        break; /* All White */
                    case 0xC2:
                        disco = -1;
                        data[5] = 0x11;
                        break; /* Z1 White. */
                    case 0xC5:
                        disco = -1;
                        data[5] = 0x13;
                        break; /* Z2 White. */
                    case 0xC7:
                        disco = -1;
                        data[5] = 0x15;
                        break; /* Z3 White. */
                    case 0xC9:
                        disco = -1;
                        data[5] = 0x17;
                        break; /* Z4 White. */
                    case 0xCB:
                        disco = -1;
                        data[5] = 0x19;
                        break; /* All Night */
                    case 0xC1:
                        disco = -1;
                        data[5] = 0x12;
                        break; /* Z1 Night */
                    case 0xC6:
                        disco = -1;
                        data[5] = 0x14;
                        break; /* Z2 Night */
                    case 0xC8:
                        disco = -1;
                        data[5] = 0x16;
                        break; /* Z3 Night */
                    case 0xCA:
                        disco = -1;
                        data[5] = 0x18;
                        break; /* Z4 Night */
                    case 0xCC:
                        disco = -1;
                        data[5] = 0x1A;
                        break;
                    default:
                        fprintf(stderr, "Unknown command %02x!\n", mesg[0]);
                        continue;
                    } /* End case command */

                    // removed command building here
                    // use json server or sth

                    /* Send command */
                    _light->send(data, 15);
                    data[6]++;
                } else {
                    fprintf(stderr, "Message has invalid size %d (expecting 2 or 3)!\n", n);
                } /* End message size check */

            } /* End handling data */

        } /* End select */

    } /* While (1) */
}
