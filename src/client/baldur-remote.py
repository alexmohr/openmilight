#!/usr/bin/env python
import argparse
import sys
import json
import socket


def main():
    parser = argparse.ArgumentParser(
        description='Command line client for openmilight baldur')

    parser.add_argument('--debug', action='store_true',
                        help='Print debugging statements')

    parser.add_argument(
        '-s', '--server', action='store',
        default='localhost',
        help='The host of the json server')

    parser.add_argument(
        '-p', '--port',
        action='store',
        default=2244,
        help='The port on which the server listens')

    parser.add_argument(
        '-r', '--remote',
        action='store',
        help='The id of the remote control')

    parser.add_argument(
        '-v', '--color',
        default=0,
        action='store',
        help='The value of the color')

    parser.add_argument(
        '-b', '--brigthness',
        default=0,
        action='store',
        help='The brigthness of the color')

    parser.add_argument(
        '-c', '--command',
        action='store',
        help='The command to send')

    parser.add_argument(
        '-n', '--numberOfResends',
        default=15,
        action='store',
        help='Defines how often a command is send in a row (how long the button is pressed)')

    args = parser.parse_args()

    data = {}
    data["remote"] = args.remote
    data["color"] = int(args.color)
    data["brightness"] = int(args.brigthness)
    data["command"] = args.command
    data["resend"] = args.numberOfResends

    # create the json string.
    data_json = None
    data_json = json.dumps(data)

    if args.debug:
        print(
            "Connection to host " + args.server + ":" +
            str(args.port) + "/remote with " + data_json)

    try:
        sockfd = socket.socket(
            socket.AF_INET,
            socket.SOCK_STREAM)

        sockfd.connect((args.server, int(args.port)))
        data_json += "\n"  # termination char!

        sockfd.send(data_json.encode())
        if args.debug:
            print("Posted data: " + data)

        read = ""
        inBuff = []
        while read != b"\n" and len(inBuff) < 1024:
            read = sockfd.recv(1)
            inBuff.append(read)

        print(bytes.join(b'', inBuff).decode('utf-8'))

    except ConnectionRefusedError:
        print("connection failed")
        sys.exit(-3)


if __name__ == "__main__":
    main()
