#include "jsonServer.h"
#include "main_test.h"
#include "openmili.h"
#include <RF24/utility/includes.h>

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <fstream>
#include <iostream>

using namespace std;

const int _port = 2244;

const string _remoteMissing = "{\"response\":\"RemoteId missing\"}\n";
const string _commandIdMissing = "{\"response\":\"CommandId missing\"}\n";
const string _cmdInvalid = "{\"response\":\"cmd invalid\"}\n";
const string _sendFailedResponse = "{\"response\":\"Failed to send data to radio.\"}\n";
const string _inparsable = "{\"response\":\"Inparsable data\"}\n";
const string _brightnessInvalid = "{\"response\":\"brightness invalid\"}\n";
const string _colorInvalid = "{\"response\":\"color invalid\"}\n";

const map<string, string> _requestResult = {
    // invalid json
    { "{\"\n", _inparsable },
    { "\n", _inparsable },

    // remote missing
    { "{\"command\": \"fff\"}\n", _remoteMissing },

    // test a numeric remote id
    { "{\"remote\": 123}\n", _commandIdMissing },

    // test a numeric command id
    { "{\"remote\": 123, \"command\": 1}\n", _sendFailedResponse },

    // test an integer  brightness.
    { "{\"remote\": 123, \"command\": 1, \"brightness\":42}\n", _sendFailedResponse },

    // test an integer  color.
    { "{\"remote\": 123, \"command\": 1, \"color\":42}\n", _sendFailedResponse },

    // test int brightness and color
    { "{\"remote\": 123, \"command\": 1, \"color\":42, \"brightness\":42}\n", _sendFailedResponse },

    // test float
    { "{\"remote\": 123, \"command\": 1, \"color\":3.14, \"brightness\":3.14}\n", _sendFailedResponse },

    // test a strange command
    { "{\"remote\": 1337, \"command\": \"*\"}\n", _cmdInvalid },

    // test hex
    { "{\"remote\": \"0x01\", \"command\": \"0x0a\"}\n", _cmdInvalid },

    // test with invalid stuff
    { "{\"remote\": \"1337\", \"command\":\"1\"}\n", _cmdInvalid },
    { "{\"remote\": \"1337\", \"command\": 1231}\n", _sendFailedResponse },
    { "{\"remote\": \"123\", \"command\": 1, \"color\":\"42\", \"brightness\":\"42\"}\n", _sendFailedResponse },
    { "{\"remote\": \"123\", \"command\": 1, \"color\":\"42\", \"brightness\":\"fail\"}\n", _brightnessInvalid },
    { "{\"remote\": \"123\", \"command\": 1, \"color\":\"fail\", \"brightness\":\"fail\"}\n", _colorInvalid },

};

TEST_CASE("api test", "[test]")
{
    // send failed because we do not open hardware this is just api test.
    string res_notSendToRadio = "{\"response\":\"" + string(json_resp::send_failed) + "\"}\n";

    OpenMilight* light = new OpenMilight();
    JsonServer* srv = new JsonServer(light);
    int openRes = srv->Open(_port);
    REQUIRE(openRes == 0);

    // use DNS to get IP address
    struct hostent* hostEntry;
    hostEntry = gethostbyname("localhost");
    REQUIRE(hostEntry);
    if (!hostEntry) {
        return;
    }

    // setup socket address structure
    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(_port);
    memcpy(&server_addr.sin_addr, hostEntry->h_addr_list[0], hostEntry->h_length);

    // create socket
    int server = socket(PF_INET, SOCK_STREAM, 0);
    REQUIRE(server > 0);
    if (server < 0) {
        perror("socket");
        return;
    }

    // connect to server
    int connected = (connect(server, (const struct sockaddr*)&server_addr, sizeof(server_addr)));
    REQUIRE(connected >= 0);
    if (connected < 0) {
        perror("connect");
        return;
    }

    for (auto const& ent1 : _requestResult) {
        string dataToSend = ent1.first;

        int sendRes = send(server, dataToSend.c_str(), dataToSend.size(), 0);
        REQUIRE(sendRes >= 0);
        if (sendRes < 0)
            continue;

        string answer = "";
        srv->GetLineFromBuffer(server, answer);
        REQUIRE(answer == ent1.second);
        if (answer != ent1.second)
            return;
    }

    const string commandPrefix = "{";
    const string commandRemote = "\"remote\": \"0xaabbcc\", \"command\":\"";
    const string commandTestSuffix = "\"}\n";
    string expectedAnswer;

    // test all commands
    for (auto const& ent1 : _commands) {
        // ent2.first is the second key
        // ent2.second is the data

        string cmd = ent1.first;
        string value = "";

        switch (_commands.at(cmd)) {
        case command_values::get_color:
        case command_values::get_brightness:
            expectedAnswer = "\{\"response\":0}\n";
            break;
        case command_values::brightness:
            value = "\"" + string(json_args::brightness) + "\":\"0xaa\",";
        case command_values::color:
            value = "\"" + string(json_args::color) + "\":\"0xaa\",";
        default:
            expectedAnswer = res_notSendToRadio;
        }

        string dataToSend = commandPrefix + value + commandRemote + ent1.first + commandTestSuffix;

        int sendRes = send(server, dataToSend.c_str(), dataToSend.size(), 0);
        REQUIRE(sendRes >= 0);
        if (sendRes < 0)
            continue;

        string answer = "";
        srv->GetLineFromBuffer(server, answer);
        REQUIRE(answer == expectedAnswer);
        if (answer != expectedAnswer)
            return;
    }

    close(server);
    srv->Close();

    // make sure we have closed
    connected = (connect(server, (const struct sockaddr*)&server_addr, sizeof(server_addr)));
    REQUIRE(connected < 0);

#if false
    // try opening again
    openRes = srv->Open(_port);
    if (openRes != 0)
        REQUIRE(gai_strerror(openRes) == 0);
    REQUIRE(openRes == 0);

    // create socket
    server = socket(PF_INET, SOCK_STREAM, 0);
    REQUIRE(server > 0);
    if (server < 0) {
        perror("socket");
        return;
    }



       // connect to server
      connected = (connect(server,(const struct sockaddr *)&server_addr,sizeof(server_addr)));
     REQUIRE (connected>= 0);
     if  (connected < 0){
         perror("connect");
         return;
     }
#endif
}
