# Baldur

# What is Baldur
Baldur is the norse god for light and pleasure. <br>
Altough you might be more interested in what this repository contains. 
This is an open implementation of the milight protocol to control various RGB bulbs. 
The application can be controlled via a simple json api and the original "wifi protcol". 

It allows the usage of all the original features of the milight bridges and offers some neat features in addition to that. 

It also contains a remote for the server which provides a neat command line interface, this currently is only able to communicate with the json api.. 
This can be used as a part of "Vidar" ( see link below ) to make your home a bit smarter. 

# References
Using code from the following repositories ( thanks to all of them ) 

https://github.com/henryk/openmili<br>
http://torsten-traenkner.de/wissen/smarthome/openmilight.php <br>
https://github.com/bakkerr/openmilight_pi <br>
https://tmrh20.github.io/RF24/RPi.html <br>
https://github.com/nlohmann/json/<br>
https://github.com/philsquared/Catch<br>

Big thanks to all of them for theier awesome work!

<br>
We only support the RF24 devices here. Sorry about that. 
If you need another driver, let me know via an issue. 
<br>
<br>
# Features
* Remote interface to be able to control the remote without a ssh session 
* Configuration
* Vidar plugin ( https://gitlab.com/alexmohr/raspberry-ir-remote )
<br>
<br>

# Dependencies
* Cmake <br>
<br>

# Installation 
Be aware that we are using is using submodules, which means you have to clone it like to following:<br> 
`$ git clone --recursive https://gitlab.com/alexmohr/openmilight`
<br>
<br>
If you forget the `--recursive` in above statement or you are updating an existing clone you need to run this:<br>
`$ git submodule init` <br>
`$ git submodule update` <br>
<br>
<br>
Enable SPI in raspi-config - Advanced Options - SPI<br>
`sudo raspi-config`<br>
<br>
After a *reboot* the relevant kernel module should be loaded<br>
`lsmod | grep spi`<br>
<br>
should list: spi_bcm2835<br>
<br>
**Install RF24**
`
cd lib/RF24
./configure --driver=SPIDEV
make 
make install
cp -r utility/ /usr/local/include/RF24
cd ../../` <br>
<br>

**Make and install baldur.**
`
mkdir build/
cd build
cmake ../src/baldur/
make 
make install`

