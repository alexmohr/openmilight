#!/bin/bash
# builds and deploys the software to my rasperry pi

HOME="/home/pi/dev/openmilight"
BUILD="$HOME/build/"

rsync -avz -e 'ssh' /home/me/dev/openmilight/src pi@baldur:~/dev/openmilight/
ssh pi@baldur "cd $BUILD && cmake ../src/baldur/"
ssh pi@baldur "cd $BUILD && make"

ssh -t pi@baldur "cd $BUILD && sudo -s && exec bash -l"

