# Protocol
This file will provide information about the protocol used.
It also explains how the json server works.

# Milight
Milight uses a very basic protocol. It is made up from 7 bytes. <br>
<br>
` "07 B0 F2 EA 7D 91 00 E9 42 1B"
	|  |		|  |  |	 |  |
	|  |        |  |  |  |  CRC	 
	|  |        |  |  |  Sequence number
	|  |        |  |  Button_of_Remote
	|  |        |  brightness
	|  |        color
	|  ID_of_Remote (3 byte) - change to your ID`<br>
	Length of the command
<br><br>
The last two bytes are CRC and the one before that is a packet ID that is incremented for each distinct packet (but kept the same for resends). the last two bytes are CRC and the one before that is a packet ID that is incremented for each distinct packet (but kept the same for resends). 
<br><br>
The least significant bit of the upper nibble in the CMD byte indicates a long press on the corresponding button: 0x11 is "all: white", 0x12 is "all: night mode", 0x13 is "group 1: white", and so on. This applies even to the disco mode buttons (but doesn't seem to trigger any functionality in the bulb), but obviously not the sliders.
<br><br>
color
<br><br>
brightness
<br>

# Command information
## Brightness
There are 32 available levels. 
Starting at 0x89 as lowest, decreasing ( and overflowing ) to 0xc1 as maximum.
Just use the 0x89 as an offest and increase value by 8. 

Baldur offers aditionals command to handle this. ( darker and brighter ).
## Color
Color is a bit more complicated. The color byte is actually a hue value. <br>
The minimum value for a color ( equal to a hue value of 0 ) is 0x89. <br>
Decremenet color by 0x8 to get the next valid hue value. 
If 0 is reached to valid values underflows to 0xF9 and keeps decrementing. <br>
You can subtract the offset before your add or subtract action and add it later again.

# Baldur
The protocoll the json server understands looks similar to this. <br>
` 
{
	"remote": "B0F2EA",
	"color": "6D",
	"brightness": "02", 
	"button": "02"
}
`<br><br>
Maybe we should add a more human readable protocol. 
`
{
	"remote:" "B0F2EA", 
	"color": "6D", 
	"brigthness": "02", 
	"command": "All_Off"
}
`


# Buttons
If you are communicating with baldur, you have to choice between 
using the hex value or sending the command name.

<table>
	<thead>
		<td>Hex value</td>
		<td>Key</td>
		<td>Json Command</td>
	</thead>
	<tbody>
		<tr>
			<td>0x01</td>
			<td>Turn off all groups</td>
			<td>all_off</td>
		</tr>
		<tr>
			<td>0x02</td>
			<td>Turn on all groups</td>
			<td>all_on</td>
		</tr>
		<tr>
			<td>0x03</td>
			<td>LED-group 1 on</td>
			<td>group_1_on</td>
		</tr>
		<tr>
			<td>0x04</td>
			<td>LED-group 1 off</td>
			<td>group_1_off</td>
		</tr>
		<tr>
			<td>0x05</td>
			<td>LED-group 2 on</td>
			<td>group_2_on</td>
		</tr>
		<tr>
			<td>0x06</td>
			<td>LED-group 2 off</td>
			<td>group_2_off</td>
		</tr>
		<tr>
			<td>0x07</td>
			<td>LED-group 3 on</td>
			<td>group_3_on</td>
		</tr>
		<tr>
			<td>0x08</td>
			<td>LED-group 3 off</td>
			<td>group_3_off</td>
		</tr>
		<tr>
			<td>0x09</td>
			<td>LED-group 4 on</td>
			<td>group_4_on</td>
		</tr>
		<tr>
			<td>0x0A</td>
			<td>LED-group 4 off</td>
			<td>group_4_off</td>
		</tr>
		<tr>
			<td>0x0B</td>
			<td>Disco mode faster</td>
			<td>disco_faster</td>
		</tr>
		<tr>
			<td>0x0C</td>
			<td>Disco mode slower</td>
			<td>disco_slower</td>
		</tr>
		<tr>
			<td>0x0D</td>
			<td>Disco mode change</td>
			<td>disco_change</td>
		</tr>
		<tr>
			<td>0x0E</td>
			<td>Brightness slider</td>
			<td>brightness</td>
		</tr>
		<tr>
			<td>0x0F</td>
			<td>Color selection wheel</td>
			<td>color</td>
		</tr>
	</tbody>
</table>
